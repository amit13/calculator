package com.ashatechsoln.amit.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class MainActivity extends AppCompatActivity {


    Button one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mul, div, cancel, eq;
    EditText disp;
    int op1,op2;
    String optr;
    boolean madd,msub,mdiv,mmul;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        one = (Button) findViewById(R.id.button1);
        two = (Button) findViewById(R.id.button2);
        three = (Button) findViewById(R.id.button3);
        four = (Button) findViewById(R.id.button4);
        five = (Button) findViewById(R.id.button6);
        six = (Button) findViewById(R.id.button5);
        seven = (Button) findViewById(R.id.button7);
        eight = (Button) findViewById(R.id.button8);
        nine = (Button) findViewById(R.id.button9);
        zero = (Button) findViewById(R.id.button10);
        add = (Button) findViewById(R.id.button11);
        sub = (Button) findViewById(R.id.button12);
        mul = (Button) findViewById(R.id.button13);
        div = (Button) findViewById(R.id.button14);
        cancel = (Button) findViewById(R.id.cancel);
        eq = (Button) findViewById(R.id.button15);

        disp = (EditText) findViewById(R.id.disp);

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(disp != null)
                {
                    disp.setText("");
                }
                // TODO Auto-generated method stub

            }
        });


        one.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"1");

            }
        });

        two.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"2");

            }
        });


        three.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"3");

            }
        });


        four.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"4");

            }
        });

        six.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"5");

            }
        });

        five.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"6");

            }
        });


        seven.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"7");

            }
        });


        eight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"8");

            }
        });


        nine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"9");

            }
        });


        zero.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"0");

            }
        });


        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(disp == null)
                {
                    disp.setText("");
                }else {
                    op1 = Integer.parseInt(disp.getText() + "");
                    madd = true;
                    disp.setText(null);

                }

            }
        });


        sub.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(disp == null)
                {
                    disp.setText("");
                }else {
                    op1 = Integer.parseInt(disp.getText() + "");
                    msub = true;
                    disp.setText(null);

                }

            }
        });



        mul.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(disp == null)
                {
                    disp.setText("");
                }else {
                    op1 = Integer.parseInt(disp.getText() + "");
                    mmul = true;
                    disp.setText(null);
                    //disp.setText(disp.getText()+"*");

                }

            }
        });


        div.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(disp == null)
                {
                    Toast.makeText(MainActivity.this,"please choose some number",Toast.LENGTH_LONG).show();
                    //disp.setText("");
                }else {
                    op1 = Integer.parseInt(disp.getText() + "");
                    mdiv = true;
                    disp.setText(null);
                    //disp.setText(disp.getText()+"/");

                }
            }
        });


        eq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                op2 = Integer.parseInt(disp.getText() + "");
                if(madd ==true)
                {

                    disp.setText(op1 + op2+"");
                    madd = false;
                }

                if(msub ==true)
                {

                    disp.setText(op1 - op2+"");
                    msub = false;
                }

                if(mmul ==true)
                {

                    disp.setText(op1 * op2+"");
                    mmul = false;
                }

                if(mdiv ==true)
                {
                    if(op2!=0)
                    {
                    disp.setText(op1 / op2+"");
                    mdiv = false;}
                    else
                        Toast.makeText(MainActivity.this,"Please choose other than 0",Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}
